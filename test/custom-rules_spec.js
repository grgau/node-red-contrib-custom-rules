var should = require("should");
var helper = require("node-red-node-test-helper");
var customRulesNode = require("../custom-rules.js");
 
helper.init(require.resolve('node-red'));
 
describe('custom-rules Node', function () {
 
  beforeEach(function (done) {
      helper.startServer(done);
  });
 
  afterEach(function (done) {
      helper.unload();
      helper.stopServer(done);
  });
 
  it('should be loaded', function (done) {
    var flow = [{ id: "n1", type: "custom-rules", name: "custom-rules" }];
    helper.load(customRulesNode, flow, function () {
      var n1 = helper.getNode("n1");
      n1.should.have.property('name', 'custom-rules');
      done();
    });
  });

  it('should be true to eq', function (done) {
    var flow = [
      { id: "n1", type: "custom-rules", name: "custom-rules", wires: [["n2"]] },
      { id: "n2", type: "helper" }
    ];
    helper.load(customRulesNode, flow, function () {
      var n2 = helper.getNode("n2");
      var n1 = helper.getNode("n1");
      n2.on("input", function (msg) {
        msg.should.have.property('payload', 'teste');
        done();
      });
      n1.receive({ payload: "teste", topic: "topic" });
    });
  })
});
