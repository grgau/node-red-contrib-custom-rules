module.exports = function(RED) {
  const operators = { 
    'eq':  function(a, b) { return a == b; },
    'neq': function(a, b) { return a != b; },
    'lt':  function(a, b) { return a < b; },
    'lte': function(a, b) { return a <= b; },
    'gt':  function(a, b) { return a > b; },
    'gte': function(a, b) { return a >= b; }
  }

  function boolAndAccumulate(conditionsArray) {
    const evaluate = conditionsArray.reduce(function(initial, cond) {
      return initial && cond;
    }, true);
    return evaluate;
  }

  function CustomRulesNode(config) {
    RED.nodes.createNode(this, config);

    this.on('input', function(msg) {
      let conditions = [];
      for (i = 0; i < config.rules.length; i++) {
        conditions.push(operators[config.rules[i].t](msg[config.rules[i].p], config.rules[i].v))
      }
      boolAndAccumulate(conditions) && this.send(msg);
    });
  }
  RED.nodes.registerType("custom-rules", CustomRulesNode);
}